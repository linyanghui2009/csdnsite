<!DOCTYPE html>
<html lang="cn">
	<head>
		<meta charset="utf-8" />
        <base href="<?php echo $system['siteRoot'];?>" />

        <title>框架测试</title>
		<meta name="description" content="测试 likyhPHP likyh团队" />
		<meta name="author" content="<?php e('author'); ?>" />
	</head>

	<body>
		<div>
			<header>
				<h1>欢迎来到likyhPHP测试兼演示界面</h1>
			</header>
			
			<div>
				欢迎来看likyhPHP演示版，希望这能帮助您快速上手。
			</div>
			
			<div>
				<a href="<?php e_url("surface", "BaseView", "index") ?>" title="进入主要页面">点击进入</a>
			</div>

			<footer>
				<p>
					<?=$result['author']; ?><br>
					&copy; Copyright  by <?= $result['author']; ?>
				</p>
			</footer>
		</div>
	</body>
</html>