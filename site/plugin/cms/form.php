<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <base href="<?php echo $system['siteRoot'];?>" />
    <title><?php echo $r['pageTitle'];?> - LikyhCMS管理系统</title>
    <link href="plugin/cms/style/common.css" rel="stylesheet"/>
    <link href="plugin/cms/style/form.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
    <header>
        <div id="company">
            <div class="title <?php echo $r['logoCopyright']?"show":"hidden";?>"><span>@LikyhCMS</span></div>
        </div>
        <div id="logo">LikyhCMS管理系统</div>
        <?php echo $r['userInfoHtml']; ?>
    </header>
    <div id="main">
        <?php echo $r['navHtml']; ?>
        <div id="content">
            <?php import_tpl('plugin/cms/contentTitle.php'); ?>
            <?php echo $r['controlHtml']; ?>
            <div id="data">
                <?php import_tpl($r['sourceTpl']);?>
            </div>
        </div>
    </div>
    <?php import_tpl('plugin/cms/footer.php');?>
</div>
</body>
</html>