<?php
// 包含系统函数
require_once 'system/lang.system.php';
require_once 'system/base.system.php';
require_once 'system/password.system.php';

$config=analysisIniFile("config/run.config.ini");

// 包含系统接口
require_once 'system/coreInterface/RunnableInterface.php';
require_once 'system/coreInterface/RouterInterface.php';
require_once 'system/coreInterface/ActivityInterface.php';
require_once 'system/coreInterface/HTMLAnalysisInterface.php';
require_once 'system/coreInterface/WebRouterInterface.php';
require_once 'system/coreInterface/HtmlCacheInterface.php';

// 包含框架核心类库
import('AppInfo');
import('WebRouter');
import('Activity');
import('Data');
import('View');