<?php
import('Custom.View.mainView');
class home extends Activity {
    /**
     * 以Task结尾的方法才是外界可以访问的，这是出于安全考虑，
     */
    function indexTask(){
        $date=date("Y-m-d");
        $author="likyh studio";

        View::displayAsHtml(array("date"=>$date,"author"=>$author), "tpl/home.php");
        echo WebRouter::init()->getURL(null,"user","index");
	}
}